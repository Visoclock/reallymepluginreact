import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import fire from "../config/fire";
import Logo from "../assets/reallyMe512.png";
import firebase from "firebase";
import Grid from "@material-ui/core/Grid";
import "./Login.css";

const Login = () => {
  const currentOfficeEmail =
    window.Office.context.mailbox.userProfile.emailAddress;

  const [loginState, setLoginState] = useState({
    email: currentOfficeEmail,
    password: "",
    fireErrors: "",
    loginBtn: true,
    loading: false,
  });

  const history = useHistory();

  const handleChange = (e) => {
    setLoginState({
      ...loginState,
      [e.target.name]: e.target.value,
    });
  };

  const handleSingleSignOn = async () => {
    try {
      var provider = new firebase.auth.OAuthProvider("microsoft.com");
      provider.setCustomParameters({
        prompt: "consent",
        tenant: "common",
      });
      const result = await firebase.auth().signInWithPopup(provider);
      // await firebase.auth().signInWithRedirect(provider);
      // const result = await firebase.auth().getRedirectResult();
      console.log("Sign in result....");
      console.log(result);
      await navigateAuthenticatedUser();
    } catch (error) {
      console.log("Error");
      console.log(error);
    }
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      setLoginState({
        ...loginState,
        loading: true,
      });
      await fire.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);
      await fire.auth().signInWithEmailAndPassword(email, password);

      await navigateAuthenticatedUser();
    } catch (error) {
      setLoginState({
        ...loginState,
        loading: false,
        fireErrors: error.message,
      });
    }
  };

  const navigateAuthenticatedUser = async () => {
    const userDoc = await fire
      .firestore()
      .collection("users")
      .doc(fire.auth().currentUser.uid)
      .get();
    const user = userDoc.data();
    setLoginState({
      ...loginState,
      loading: false,
    });
    history.push("/", { user });
  };

  const { email, password, fireErrors, loading } = loginState;

  let errorNotification = fireErrors ? (
    <div className="alert error">{fireErrors}</div>
  ) : null;
  return (
    <div className="container">
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="center"
      >
        <img
          src={Logo}
          alt="logo"
          width="50"
          height="50"
          style={{ marginTop: "20px" }}
        ></img>
        <span class="top-name">reallyMe</span>
        <span class="sign-header">Sign Into Account</span>
        <form onSubmit={handleLogin} style={{ width: "80%" }}>
          <Grid
            style={{ marginTop: "20px", width: "100%" }}
            container
            direction="column"
            justify="flex-start"
            alignItems="center"
          >
            {errorNotification}
            <div class="form-group">
              <span class="form-label">Email Address</span>
              <input
                disabled
                type="text"
                name="email"
                class="form-input"
                placeholder="Email"
                onChange={handleChange}
                value={email}
              />
            </div>
            <div class="form-group" style={{ marginTop: "10px" }}>
              <span class="form-label">Password</span>
              <input
                type="password"
                name="password"
                class="form-input"
                placeholder="Password"
                onChange={handleChange}
                value={password}
              />
            </div>
            <div
              style={{
                width: "100%",
                marginTop: "10px",
                marginBottom: "10px",
              }}
            >
              <span class="link">Forgot Password?</span>
            </div>
            <button class="form-button">
              {" "}
              {loading ? "Signing In..." : "Sign In"}
            </button>

            <div style={{ marginTop: "10px" }}>
              <a
                href="https://www.reallyme.live"
                target="_blank"
                rel="noopener noreferrer"
              >
                Get the reallyMe App
              </a>
            </div>
          </Grid>
        </form>
        <div className="btn" onClick={handleSingleSignOn}>
          Sign in with Active Directory
        </div>
      </Grid>
    </div>
  );
};

export default Login;
