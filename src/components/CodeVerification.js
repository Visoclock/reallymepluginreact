import { Grid, Card, CardContent, Avatar } from "@material-ui/core";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import moment from "moment";
import CodeVerificationCard from "./CodeVerificationCard";
import ProcessAnimation from "./sub/ProcessAnimation";
import * as React from "react";
import "./CodeVerification.css";
import Splash from "./Splash";

export default class CodeVerification extends React.Component {
  constructor(props) {
    super(props);

    //console.log("verification constructor called");
    this.state = {
      verifying: false,
      isWrongSender: false,
      status: null,
      result: null,
      error: "",
    };

    this.firestoreSnapshot = null;
    this.currentOfficeEmail =
      window.Office.context.mailbox.userProfile.emailAddress;
  }

  componentDidMount() {
    this.handleVerify();
  }

  async handleVerify() {
    this.setState({
      verifying: true,
      error: "",
    });
    try {
      //   const code = "Uujf4vSNRi";
      const officeItem = window.Office.context.mailbox.item;
      officeItem.body.getAsync(
        window.Office.CoercionType.Text,
        async (result) => {
          if (result.status === "succeeded") {
            //console.log(result.value);
            let re = /#%RMC=[a-zA-Z0-9]{10}/;
            let matches = re.exec(result.value);

            console.log("FROM ----");
            console.log(officeItem.from.emailAddress);
            if (!matches) {
              this.setState({
                verifying: false,
                error: "No reallyMe code detected in message",
              });
            } else {
              const code = matches[0].substring(6);
              //console.log(code);
              // const code = "PbkLarSqMO";
              const requestOptions = {
                method: "POST",
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ signatureCode: code }),
              };

              const result = await fetch(
                "https://us-central1-reallyme-66efd.cloudfunctions.net/app/signature/verify",
                requestOptions
              );

              //console.log(result);

              //400 invalid stuff
              let error = "Failed to verify";
              let userDetails;
              if (result.status === 403) {
                error = "Verification code expired";
              } else if (result.status === 404) {
                error = "Verification code not found";
              } else if (result.status === 200) {
                error = "";
                userDetails = await result.json();

                if (this.currentOfficeEmail !== officeItem.from.emailAddress) {
                  error =
                    "The signature's owner failed to match the email's sender";
                  userDetails = null;
                }
              }

              this.setState({
                verifying: false,
                result: userDetails,
                error,
              });
            }
          } else {
            this.setState({
              verifying: false,
              error: "Failed to read message",
            });
          }
        }
      );
    } catch (error) {
      //console.log(error);
      this.setState({
        verifying: false,
        error: "Failed to verify",
      });
    }
  }

  render() {
    const result = this.state.result;

    const errorDisplay = this.state.error ? (
      <Grid container direction="column" justify="center" alignItems="center">
        <ProcessAnimation status="failure" />
        <div className="error-text">{this.state.error}</div>
      </Grid>
    ) : null;

    const verifiedCard = this.state.result ? (
      <CodeVerificationCard result={result} />
    ) : null;

    if (this.state.verifying) {
      return <Splash title="verifying token..." />;
    }
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{ minHeight: "90vh" }}
      >
        {errorDisplay}
        {verifiedCard}
      </Grid>
    );
  }
}
