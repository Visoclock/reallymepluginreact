import * as React from "react";
import { Grid } from "@material-ui/core";
import Logo from "../assets/reallyMe512.png";

export default class Splash extends React.Component {
  render() {
    const { title } = this.props;

    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{ minHeight: "99vh", backgroundColor: "#a94dbd" }}
      >
        <img
          src={Logo}
          alt="logo"
          width="50"
          height="50"
          style={{ marginTop: "20px" }}
        ></img>
        <span
          style={{
            color: "white",
            fontWeight: "bold",
            display: "inline-block",
            marginTop: "20px",
            textAlign: "center",
          }}
        >
          {title ? title : "loading..."}
        </span>
      </Grid>
    );
  }
}
