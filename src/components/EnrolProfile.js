import React, { useState } from "react";
import fire from "../config/fire";
import { Redirect } from "react-router-dom";
import { Grid, Card, CardContent, Avatar } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import Compressor from "compressorjs";
import "./EnrolProfile.css";

const EnrolProfile = (props) => {
  const propUser = props.location.state.user;
  // const propUser = {
  //   uuid: "gcCuIg5E0veYOnEs1Ym74fr4tYj1",
  //   guessed_age: 0,
  //   username: "Nony Bright O",
  //   id_checked: false,
  //   ref_code: "44646",
  //   mail_sends: 3,
  //   telephones: [],
  //   member_country: null,
  //   vocus: true,
  //   org_id: "J163VX4XJGbG1TFj4fjm",
  //   image_url:
  //     "https://firebasestorage.googleapis.com/v0/b/reallyme-66efd.appspot.com/o/teddy-5119450_640.jpg?alt=media&token=e4dcb4d8-1241-471a-8a87-4a65c5c28d18",
  // };

  const [user, setUser] = useState(propUser);
  const [fileInput, setFileInput] = useState(null);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [redirectToEnrol, setRedirectToEnrol] = useState(false);

  const updateUserImage = async (event) => {
    try {
      const file = event.target.files[0];
      if (!file) {
        throw new Error("No file to upload");
      }
      setLoading(true);
      new Compressor(file, {
        quality: 0.7,
        async success(result) {
          //console.log(result);
          try {
            const imageRef = await fire
              .storage()
              .ref(`profilepics/${user.uuid}/image.jpeg`)
              .put(result);
            const imageUrl = await imageRef.ref.getDownloadURL();
            //console.log(imageUrl);

            await fire.firestore().collection("users").doc(user.uuid).update({
              image_url: imageUrl,
            });
            setUser({ ...user, image_url: imageUrl });
            setLoading(false);
            setTimeout(() => {
              setRedirectToEnrol(true);
            }, 3000);
          } catch (error) {
            setLoading(false);
            setErrorMessage("Failed to change Image");
          }
        },
        error(err) {
          throw new Error("Could not compress Image");
        },
      });
    } catch (error) {
      //console.log("Error occured");
      //console.log(error);
      setLoading(false);
      setErrorMessage("Failed to change Image");
    }
  };

  if (redirectToEnrol) {
    return <Redirect to={{ pathname: "/enrol", state: { user } }} />;
  }
  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      style={{ minHeight: "90vh" }}
    >
      <Card style={{ margin: "20px", borderRadius: "20px" }}>
        <CardContent>
          <Grid
            direction="column"
            container
            alignItems="center"
            justify="center"
          >
            <div className="name-holder">Hello {user.username}</div>
            <div class="welcome-info">
              <span>
                Welcome to reallyMe. Let’s take you through one final step
              </span>
            </div>
            <Avatar
              alt="avatar"
              src={user.image_url}
              style={{ width: "120px", height: "120px", margin: "10px 0" }}
            />
            <div class="btn" onClick={() => fileInput.click()}>
              {loading ? (
                <CircularProgress
                  style={{ color: "white", width: "10px", height: "10px" }}
                />
              ) : (
                "Choose Profile Picture"
              )}
            </div>
            <span
              className="skip-btn"
              onClick={() => {
                setRedirectToEnrol(true);
              }}
            >
              Skip this
            </span>
            <input
              style={{ display: "none" }}
              type="file"
              accept="image/png, image/jpeg"
              onChange={updateUserImage}
              ref={(fileInput) => setFileInput(fileInput)}
            />
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default EnrolProfile;
