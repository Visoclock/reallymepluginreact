import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import fire from "../config/fire";
import Grid from "@material-ui/core/Grid";
import "./SignatureList.css";
import { IconButton, Typography, Toolbar, AppBar } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import SignatureUserItem from "./sub/SignatureUserItem";

const SignatureUserList = (props) => {
  const [loading, setLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");
  const [signatureUsers, setSignatureUsers] = useState([]);

  const history = useHistory();
  const params = useParams();

  //valid signature -47XmrikVZI

  useEffect(() => {
    const fetchSignatures = async () => {
      try {
        setLoading(true);

        const signatureUsersDoc = await fire
          .firestore()
          .collection("signatureUsers")
          .doc(params.signatureCode)
          .collection("users")
          .orderBy("used_on", "desc")
          .get();

        //console.log("signaturesdoc..");
        //console.log(signatureUsersDoc);

        if (signatureUsersDoc.empty) {
          setSignatureUsers([]);
        } else {
          let gottenSignatureUsers = [];
          let returnedSignatureUsers = [];
          signatureUsersDoc.forEach((doc) => {
            gottenSignatureUsers.push({
              ...doc.data(),
            });
          });

          //console.log('----all gotten signatures')
          //console.log(gottenSignatureUsers)

          for (let i = 0; i < gottenSignatureUsers.length; i++) {
            const userDoc = await fire
              .firestore()
              .collection("users")
              .doc(props.user.uuid)
              .get();
            //console.log("----user-doc");
            //console.log(userDoc);
            if (userDoc !== null) {
              returnedSignatureUsers.push({
                code: params.signatureCode,
                user: userDoc.data(),
                dateUsed: gottenSignatureUsers.used_on,
              });
            }
          }
          setSignatureUsers(returnedSignatureUsers);
        }
        setLoading(false);
      } catch (error) {
        //console.log(error);
        setLoading(false);
        setErrorMessage("error getting signatures");
      }
    };
    fetchSignatures();
  }, [params.signatureCode, props.user.uuid]);

  const progress = loading ? <div>loading item..</div> : null;
  const UserList = () => {
    if (!loading && !errorMessage) {
      if (Array.isArray(signatureUsers) && signatureUsers.length !== 0) {
        const signatureItems = signatureUsers.map((signatureUser) => (
          <SignatureUserItem signatureUser={signatureUser} key={signatureUser.dateUsed}/>
        ));
        return <div>{signatureItems}</div>;
      } else {
        return <div>Token not used yet</div>;
      }
    }
    return null;
  };

  return (
    <div>
      <AppBar position="relative" style={{ backgroundColor: "#9b27af" }}>
        <Toolbar variant="dense">
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={() => {
              history.goBack();
            }}
          >
            <ArrowBackIcon />
          </IconButton>
          <Typography variant="h6">Users of {params.signatureCode}</Typography>
        </Toolbar>
      </AppBar>
      <div>{progress}</div>
      <UserList />
    </div>
  );
};

export default SignatureUserList;
