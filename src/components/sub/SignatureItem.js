import React from "react";
import { useHistory } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import "./SignatureItem.css";

const SignatureItem = (props) => {
  const history = useHistory();

  return (
    <div>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="center"
      >
        {JSON.stringify(props.signature)}
      </Grid>
      <button
        onClick={() => history.push(`/signature-users/${props.signature.code}`)}
      >
        Display users
      </button>
    </div>
  );
};

// {"user_id":"gcCuIg5E0veYOnEs1Ym74fr4tYj1","code":"l756H","expiry_date_time":"2020-10-03T12:52:13.047Z","creation_date_time":"2020-09-30T12:52:13.047Z","link":"https://reallyme.page.link/f3aK","headline":"send picture","active":true,"id":"l756H","location":{"latitude":0,"longitude":0}}

export default SignatureItem;
