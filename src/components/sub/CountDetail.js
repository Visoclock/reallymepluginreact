import * as React from "react";
import { Grid, Card, CardContent } from "@material-ui/core";
import "./CountDetail.css";

export default class CountDetail extends React.Component {
  render() {
    const { icon, title, count, description } = this.props;

    return (
      <Card style={{ margin: "5px", borderRadius: "10px", boxShadow: "none" }}>
        <CardContent>
          <Grid
            direction="column"
            container
            alignItems="center"
            justify="center"
          >
            <img src={icon} alt="cnt" width="30px" height="30px" />
            <span class="counter-title">{title}</span>
            <div class="counter-border">
              <span class="counter-count">{count}</span>
            </div>
            <span class="counter-description">{description}</span>
          </Grid>
        </CardContent>
      </Card>
    );
  }
}
