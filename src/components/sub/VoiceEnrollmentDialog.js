import React, { useState, useEffect } from "react";
import { Grid } from "@material-ui/core";
import "./VoiceEnrollmentDialog.css";
import * as RecordRTC from "recordrtc";
import "./VoiceEnrollmentDialog.css";
import fire from "../../config/fire";
import CircularProgress from "@material-ui/core/CircularProgress";
import ProcessAnimation from "./ProcessAnimation";

const VoiceEnrollmentDialog = (props) => {
  const { action, profileId } = props;
  const recordTime = 5000; // 5 seconds
  const maxAttempts = action === "enrol" ? 3 : 1;
  const [passPhrase, setPassPhrase] = useState("Waiting for phrase...");
  const [recording, setRecording] = useState(false);
  const [recordTimeLeft, setRecordTimeLeft] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [showFinalSuccess, setShowFinalSuccess] = useState(false);
  const [showIntermediateSuccess, setShowIntermediateSuccess] = useState(false);
  const [showIntermediateFailure, setShowIntermediateFailure] = useState(false);
  const [remainingEnrollmentsCount, setRemainingEnrollmentsCount] = useState(
    maxAttempts
  );

  let recorder;

  useEffect(() => {
    const getPhrase = async () => {
      try {
        const userToken = await fire.auth().currentUser.getIdToken();
        setLoading(true);
        const getPhrase = {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${userToken}`,
          },
        };

        const phraseResult = await fetch(
          `https://us-central1-reallyme-66efd.cloudfunctions.net/app/speaker/dependent/list-phrases/en-US`,
          getPhrase
        );

        if (phraseResult.status === 200) {
          const phrases = await phraseResult.json();
          setPassPhrase(phrases[Math.floor(Math.random() * 7)].passPhrase);
        }
        setLoading(false);
      } catch (error) {}
    };

    getPhrase();
  }, []);

  const recordVoice = async () => {
    startRecording();
    const timeLeftInterval = setInterval(() => {
      setRecordTimeLeft((recordTimeLeft) => recordTimeLeft - 100);
    }, 100);
    setTimeout(async () => {
      stopRecording();
      clearInterval(timeLeftInterval);
    }, 5000);
  };

  const captureMicrophone = async (callback) => {
    try {
      const mic = await window.navigator.mediaDevices.getUserMedia({
        audio: true,
        video: false,
      });
      callback(mic);
    } catch (error) {
      alert("Unable to capture your microphone. Please check console logs.");
      console.error(error);
    }
  };
  const startRecording = () => {
    captureMicrophone((mediaStream) => {
      recorder = new RecordRTC(mediaStream, {
        type: "audio",
        mimeType: "audio/wav",
        recorderType: RecordRTC.StereoAudioRecorder,
        desiredSampRate: 16000,
        numberOfAudioChannels: 1,
      });
      recorder.startRecording();
      setRecording(true);
      setShowIntermediateFailure(false);
      setShowIntermediateSuccess(false);
      setRecordTimeLeft(recordTime);
    });
  };
  const stopRecording = () => {
    recorder.stopRecording(async () => {
      setRecording(false);
      const userToken = await fire.auth().currentUser.getIdToken();
      try {
        setLoading(true);
        let blob = recorder.getBlob();

        const requestOptions = {
          method: "POST",
          headers: {
            "Content-Type": "audio/wav",
            // "Content-Type": "application/json",
            Authorization: `Bearer ${userToken}`,
          },
          body: blob,
        };
        const endPoint =
          action === "enrol"
            ? `https://us-central1-reallyme-66efd.cloudfunctions.net/app/speaker/dependent/${profileId}/create-enrollments`
            : `https://us-central1-reallyme-66efd.cloudfunctions.net/app/speaker/dependent/verify-profile`;

        const result = await fetch(endPoint, requestOptions);
        if (result.status === 201 || result.status === 200) {
          if (action === "enrol") {
            const response = await result.json();
            const remainingCount = response.remainingEnrollmentsCount;
            setRemainingEnrollmentsCount(remainingCount);
            if (remainingCount === 0) {
              await enrolUser();
            } else {
              //enroll user
              setShowIntermediateSuccess(true);
            }
          } else {
            // const response = await result.json();
            // //console.log(response);
            handleVerificationCompleted();
          }
        } else {
          if (result.status === 400) {
            const response = await result.json();
            //console.log(response);
          }
          setShowIntermediateFailure(true);
        }
        setLoading(false);
      } catch (error) {
        //console.log(error);
        setLoading(false);
        //console.log("processing error");
      }
    });
  };

  const enrolUser = async () => {
    const userToken = await fire.auth().currentUser.getIdToken();
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userToken}`,
      },
    };
    const endPoint = `https://us-central1-reallyme-66efd.cloudfunctions.net/app/speaker/dependent/enrolled`;
    const result = await fetch(endPoint, requestOptions);

    if (result.status === 200) {
      handleVerificationCompleted();
    } else {
      //handle error
      //console.log(result);
      // TODO: handle error in a mins
    }
  };

  const handleVerificationCompleted = () => {
    setShowFinalSuccess(true);
    setTimeout(() => {
      props.onSuccess();
    }, 2000);
  };

  const ButtonMessage = () => {
    if (maxAttempts === remainingEnrollmentsCount) {
      return <span>Tap to speak</span>;
    } else if (remainingEnrollmentsCount === 2) {
      return <span>Repeat Phrase</span>;
    } else if (remainingEnrollmentsCount === 1) {
      return <span>One Last Time</span>;
    } else {
      return <span>Tap to speak</span>;
    }
  };
  const RecordingTimer = () => {
    if (recording) {
      // return <span>Time left: {recordTimeLeft} </span>;
      return (
        <div className="timer-holder">
          <CircularProgress
            variant="static"
            value={((recordTime - recordTimeLeft) / recordTime) * 100}
            style={{
              width: "80px",
              height: "80px",
              position: "absolute",
              color: "#5bb543",
            }}
          />
          <span className="timer-time">
            00:0{Math.floor(recordTimeLeft / 1000)}{" "}
          </span>
        </div>
      );
    }
    return null;
  };

  const intermediateSuccessDisplay = showIntermediateSuccess ? (
    <span className="success">SUCCESS!</span>
  ) : null;
  const intermediateFailureDisplay = showIntermediateFailure ? (
    <span className="failure">FAILED!</span>
  ) : null;
  const errorMessageDisplay = errorMessage ? <span>{errorMessage}</span> : null;

  const loadingDisplay = loading ? (
    <CircularProgress
      style={{
        color: "#9b27af",
      }}
    />
  ) : null;
  if (showFinalSuccess) {
    return (
      <Grid container direction="column" justify="center" alignItems="center">
        <ProcessAnimation status="success" />
      </Grid>
    );
  }

  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      className="holder"
    >
      {errorMessageDisplay}
      <span class="phrase">
        <b>{passPhrase}</b>
      </span>
      <span className="explainer">
        {action === "enrol"
          ? "Repeat the phrase above three separate times."
          : "Say the phrase above once"}
      </span>
      <RecordingTimer />
      {intermediateSuccessDisplay}
      {intermediateFailureDisplay}
      {loadingDisplay}
      <div className="btn" onClick={recordVoice}>
        <ButtonMessage />
      </div>
    </Grid>
  );
};

export default VoiceEnrollmentDialog;
