// import React, { useState, useEffect } from "react";
// import fire from "../config/fire";
// import Webcam from "react-webcam";

// const FaceEnrol = (props) => {
//   const [record, setRecord] = useState(false);
//   const videoConstraints = {
//     width: 450,
//     height: 720,
//     facingMode: "user",
//   };

//   const webcamRef = React.useRef(null);

//   const capture = React.useCallback(async () => {
//     const imageSrc = webcamRef.current.getScreenshot();

//     const userId = "gcCuIg5E0veYOnEs1Ym74fr4tYj1";

//     //console.log(userId);

//     const userToken =
//       "eyJhbGciOiJSUzI1NiIsImtpZCI6IjBlM2FlZWUyYjVjMDhjMGMyODFhNGZmN2RjMmRmOGIyMzgyOGQ1YzYiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vcmVhbGx5bWUtNjZlZmQiLCJhdWQiOiJyZWFsbHltZS02NmVmZCIsImF1dGhfdGltZSI6MTYwMjg2NTY4MSwidXNlcl9pZCI6ImdjQ3VJZzVFMHZlWU9uRXMxWW03NGZyNHRZajEiLCJzdWIiOiJnY0N1SWc1RTB2ZVlPbkVzMVltNzRmcjR0WWoxIiwiaWF0IjoxNjAyOTUwMDk2LCJleHAiOjE2MDI5NTM2OTYsImVtYWlsIjoibm9ueWJyaWdodG9AZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsibm9ueWJyaWdodG9AZ21haWwuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.JagkeTqNztwuE99ZdPHkhiiTSzsr4k13fWFNG44JtVULIlRUvr3Mj1bGIrhoPjkchvxMGekdMIgkexiOL3Fh9l2WXNmstkM7sPexEPTPBZyQNYPG3X9uDlUFj23J27o2TWX63uyWWXVN_BVkz8vVca71V-1nqXt2zGKBeD8ispXp3-oHzuedvsTRPrYOOx92lpR8LGWgaZf-cFtJLJIk-ijEAmNGKnPA-Kb5I9koSSW37z5nmS_Scmpe7odtzlq0hxGcL0tC-0df6Rgy0RiHnn7gtxtgEu-tLLCJFjcLPRRAIBGm_puBndKhOQjVKGHBng8BN3oSZ2N8F6Q069qgnw";

   
//     //console.log("Image src here");
//     //console.log(imageToSend);

//     const requestOptions = {
//       method: "POST",
//       headers: {
//         "Content-Type": "application/json",
//         Authorization: "Bearer " + userToken,
//       },
//       body: JSON.stringify({ faceImage: imageToSend }),
//     };

//     //console.log("Sending image for enrollment...");

//     const result = await fetch(
//       "https://us-central1-reallyme-66efd.cloudfunctions.net/app/face/enroll",
//       requestOptions
//     );

//     //console.log("Show result");
//     //console.log(await result.json());
//   }, [webcamRef]);

//   const onData = (recordedBlob) => {
//     //console.log("chunk of real-time data is: ", recordedBlob);
//   };

//   const onStop = (recordedBlob) => {
//     //console.log("recordedBlob is: ", recordedBlob);
//   };

//   return (
//     <div>
//       Enrollment screen
//       <Webcam
//         audio={false}
//         height={350}
//         ref={webcamRef}
//         screenshotFormat="image/jpeg"
//         width={200}
//         videoConstraints={videoConstraints}
//       />
//       <button onClick={capture}>Capture photo</button>
//       ----------------------------------
//       <button
//         onClick={() => {
//           setRecord(true);
//         }}
//         type="button"
//       >
//         Start
//       </button>
//       <button
//         onClick={() => {
//           setRecord(false);
//         }}
//         type="button"
//       >
//         Stop
//       </button>
//     </div>
//   );
// };

// export default FaceEnrol;
