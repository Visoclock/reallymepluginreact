import * as React from "react";
// import TopImage from "../../assets/topimage.jpg";
import { Grid } from "@material-ui/core";
import "./InfoDisplay.css";

const InfoDisplay = (props) => {
  return (
    <Grid direction="column" container alignItems="center" justify="center">
      {/* <img src={TopImage} alt="topimage" className="top-image" /> */}
      <div className="top-image"></div>
      <div className="info-wrapper">
        <Grid direction="column" container alignItems="center" justify="center">
          <span className="info-title">{props.title}</span>
          <span className="info-content">{props.description}</span>
        </Grid>
      </div>
    </Grid>
  );
};
export default InfoDisplay;
