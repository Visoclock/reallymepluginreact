import * as React from "react";
import { Grid, CardContent } from "@material-ui/core";
import { ErrorOutline } from "@material-ui/icons";
import "./QuickAction.css";

export default class QuickAction extends React.Component {
  render() {
    const { icon, title, description, buttonText, color, onActionClick } = this.props;

    return (
      <div
        style={{
          backgroundColor: color,
        }}
        className="quick-action"
        onClick={onActionClick}
      >
        <CardContent style={{ padding: "5px" }}>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
          >
            <ErrorOutline style={{ color: "#ffffff" }} fontSize="small" />
          </Grid>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="flex-start"
          >
            <Grid item xs={2}>
              <img src={icon} alt="qicon" width="25px" height="25px" />
            </Grid>
            <Grid item xs={10} style={{ paddingLeft: "8px" }}>
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
              >
                <Grid
                  item
                  style={{
                    fontSize: "0.6rem",
                    fontWeight: "bold",
                    marginBottom: "5px",
                  }}
                >
                  <span>{title}</span>
                </Grid>
                <Grid
                  item
                  style={{
                    marginBottom: "5px",
                  }}
                >
                  <span>{description}</span>
                </Grid>
                <Grid item>
                  <div className="quick-button">{buttonText}</div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </div>
    );
  }
}
