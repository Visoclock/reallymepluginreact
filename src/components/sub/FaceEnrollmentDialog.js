import React, { useState, useEffect } from "react";
import { Grid } from "@material-ui/core";
import "./VoiceEnrollmentDialog.css";
import * as RecordRTC from "recordrtc";
import "./VoiceEnrollmentDialog.css";
import CircularProgress from "@material-ui/core/CircularProgress";
import ProcessAnimation from "./ProcessAnimation";
import fire from "../../config/fire";
import Webcam from "react-webcam";

const FaceEnrollmentDialog = (props) => {
  const { action } = props;
  const [loading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);
  const [showFinalSuccess, setShowFinalSuccess] = useState(false);

  const videoConstraints = {
    width: 450,
    height: 500,
    facingMode: "user",
  };

  const webcamRef = React.useRef(null);

  const handleFaceEnrol = async () => {
    try {
      //console.log("HANDLE FACE ENROLL");
      const userToken = await fire.auth().currentUser.getIdToken();
      //console.log(userToken);
      setLoading(true);
      setErrorMessage("");
      const imageBase64 = getImageBase64();
      const requestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + userToken,
        },
        body: JSON.stringify({ faceImage: imageBase64 }),
      };

      const endPointAction = action === "enrol" ? "enroll" : "verify";
      const result = await fetch(
        `https://us-central1-reallyme-66efd.cloudfunctions.net/app/face/${endPointAction}`,
        requestOptions
      );
      const output = await result.json();
      //console.log("FACE RESULT....");
      //console.log(result);
      //console.log(output);
      if (result.status === 200) {
        setShowFinalSuccess(true);
        setTimeout(() => {
          props.onSuccess();
        }, 3000);
      } else {
        setErrorMessage("Failed");
      }
      setLoading(false);
    } catch (error) {
      //console.log(error);
      setLoading(false);
      setErrorMessage("FAILED");
    }
  };

  const getImageBase64 = () => {
    const imageSrc = webcamRef.current.getScreenshot();
    return imageSrc.split(",")[1]; //The server doesnt need the base64 image description, so we strip it out.
  };

  const loadingDisplay = loading ? (
    <CircularProgress
      style={{
        color: "#9b27af",
      }}
    />
  ) : null;

  const errorDisplay = errorMessage ? (
    <span className="failure">FAILED!</span>
  ) : null;

  if (showFinalSuccess) {
    return (
      <Grid container direction="column" justify="center" alignItems="center">
        <ProcessAnimation status="success" />
      </Grid>
    );
  }

  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      className="holder"
    >
      <Webcam
        audio={false}
        height={230}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        width={200}
        videoConstraints={videoConstraints}
      />
      {errorDisplay}
      {loadingDisplay}
      <div className="btn" onClick={handleFaceEnrol}>
        {action === "enrol" ? "Enroll Face" : "Verify Face"}
      </div>
    </Grid>
  );
};

export default FaceEnrollmentDialog;
