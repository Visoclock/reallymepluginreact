import React from "react";
import Grid from "@material-ui/core/Grid";
import "./SignatureUserItem.css";

const SignatureUserItem = (props) => {
  return (
    <div>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="center"
      >
        {JSON.stringify(props.signatureUser)}
        ---------
      </Grid>
    </div>
  );
};

export default SignatureUserItem;
