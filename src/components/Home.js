import React, { useState, useEffect } from "react";
import fire from "../config/fire";
import moment from "moment";
import { Link } from "react-router-dom";
import BadgeIcon from "../assets/icons/badge.svg";
import OrganizationIcon from "../assets/icons/organization.svg";
import SignatureIcon from "../assets/icons/signature.svg";
import VerificationIcon from "../assets/icons/verification.svg";
import VerificationWhiteIcon from "../assets/icons/verification_white.svg";
import SignatureWhiteIcon from "../assets/icons/signature_white.svg";
import CircularProgress from "@material-ui/core/CircularProgress";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import VoiceEnrollmentDialog from "./sub/VoiceEnrollmentDialog";
import FaceEnrollmentDialog from "./sub/FaceEnrollmentDialog";
import { Menu } from "@material-ui/icons";
import CountDetail from "./sub/CountDetail";
import QuickAction from "./sub/QuickAction";
import InfoDisplay from "./sub/InfoDisplay";
import ProcessAnimation from "./sub/ProcessAnimation";
import CodeVerificationCard from "./CodeVerificationCard";
import "./Home.css";
import {
  SwipeableDrawer,
  Grid,
  Avatar,
  IconButton,
  Dialog,
  DialogTitle,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";

const Home = (props) => {
  const { user } = props.location.state;
  const [creating, setCreating] = useState(false);
  const [verifyingMobile, setVerifyingMobile] = useState(false);
  const [displayVerificationSuccess, setDisplayVerificationSuccess] = useState(
    false
  );
  const [displayVerificationFailure, setDisplayVerificationFailure] = useState(
    false
  );
  const [displayCodeGenerator, setDisplayCodeGenerator] = useState(false);
  // const [verificationCancelled, setVerificationCancelled] = useState(false);
  // const [allowGenerateCode, setAllowGenerateCode] = useState(false);
  const [generatingQr, setGeneratingQr] = useState(false);
  const [generatedQrResponse, setGeneratedQrResponse] = useState(null);
  const [headline, setHeadline] = useState("");
  const [verificationCode, setVerificationCode] = useState("");
  const [verifyingCode, setVerifyingCode] = useState(false);
  const [verificationCodeResult, setVerificationCodeResult] = useState("");
  const [verificationCodeError, setVerificationCodeError] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [generatorDialogOpen, setGeneratorDialogOpen] = useState(false);
  const [verificationDialogOpen, setVerificationDialogOpen] = useState(false);
  const [qrDialogOpen, setQrDialogOpen] = useState(false);
  const [generatorInfoDialogOpen, setGeneratorInfoDialogOpen] = useState(false);
  const [verificationInfoDialogOpen, setVerificationInfoDialogOpen] = useState(
    false
  );
  const [verificationCount, setVerificationCount] = useState(null);
  const [organizationCount, setOrganizationCount] = useState(null);
  const [userOrganization, setUserOrganization] = useState(null);
  const [showFaceEnrol, setShowFaceEnrol] = useState(false);
  const [showVoiceEnrol, setShowVoiceEnrol] = useState(false);
  const [faceVoiceDialogOpen, setfaceVoiceDialogOpen] = useState(false);

  useEffect(() => {
    const getProfileDetails = async () => {
      fire
        .firestore()
        .collection("signature")
        .where("user_id", "==", user.uuid)
        .onSnapshot((snapshot) => {
          //console.log(snapshot);
          setVerificationCount(snapshot.docs.length);
        });

      try {
        const userOrgId = user.org_id;
        if (userOrgId) {
          const userOrganizationDoc = await fire
            .firestore()
            .collection("Organizations")
            .doc(userOrgId)
            .get();
          const userOrganization = userOrganizationDoc.data();

          //console.log("-------ORGANIZATION----");
          //console.log(userOrganization);

          setOrganizationCount(1);
          //console.log("set user organization..");
          setUserOrganization(userOrganization);
        } else {
          setOrganizationCount(0);
        }
      } catch (error) {}
    };
    getProfileDetails();
  }, [user]);

  const handleGenerate = async () => {
    setCreating(true);
    setGeneratedQrResponse(null);
    setErrorMessage("");
    try {
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ uuid: user.uuid }),
      };

      await fetch(
        "https://us-central1-reallyme-66efd.cloudfunctions.net/app/verification/create",
        requestOptions
      );

      setCreating(false);
      if (userOrganization && userOrganization.use_plugin) {
        setGeneratorDialogOpen(false);
        setfaceVoiceDialogOpen(true);
        setShowFaceEnrol(true);
      } else {
        watchMobileVerification();
      }
    } catch (error) {
      setCreating(false);
      setErrorMessage("Failed to generate");
    }
  };

  const watchMobileVerification = () => {
    setVerifyingMobile(true);
    setErrorMessage("");

    fire
      .firestore()
      .collection("verificationRequest")
      .doc(user.uuid)
      .onSnapshot((doc) => {
        const status = doc.data().status;
        if (status === "VERIFIED") {
          setVerifyingMobile(false);
          window.Office.context.mailbox.item.subject.getAsync((result) => {
            setDisplayVerificationSuccess(true);
            if (result.status === "succeeded") {
              setTimeout(() => {
                //console.log("setting headline to " + result.value);
                setHeadline(result.value);
                setDisplayVerificationSuccess(false);
                if (result.value) {
                  generateQr(result.value);
                } else {
                  setDisplayCodeGenerator(true);
                }
              }, 3000);
            } else {
              setTimeout(() => {
                setDisplayCodeGenerator(true);
              }, 3000);
            }
          });
        } else if (status === "CANCEL" || status === "EXPIRED") {
          setVerifyingMobile(false);
          setDisplayVerificationFailure(true);
        }
      });
  };

  const generateQr = async (formSubject) => {
    try {
      setGeneratingQr(true);
      //console.log("THe current headline is ...");
      //console.log(headline);
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          uuid: user.uuid,
          headline: formSubject || headline,
        }),
      };

      const result = await fetch(
        "https://us-central1-reallyme-66efd.cloudfunctions.net/app/signature/create",
        requestOptions
      );

      let generatedQrResponse = await result.json();
      console.log("the qr response");
      console.log(generatedQrResponse);
      setGeneratedQrResponse(generatedQrResponse);
      setGeneratingQr(false);
      setGeneratorDialogOpen(false);
      setQrDialogOpen(true);
    } catch (error) {
      console.log("error on qr side");
      console.log(error);
    }
  };

  const handleAppendSignature = async (e) => {
    appendSignature();
  };

  const handleFaceEnrollmentSuccess = () => {
    setShowFaceEnrol(false);
    setShowVoiceEnrol(true);
  };
  const handleVoiceEnrollmentSuccess = () => {
    window.Office.context.mailbox.item.subject.getAsync((result) => {
      setShowVoiceEnrol(false);
      if (result.status === "succeeded") {
        //console.log("subject result...");
        //console.log(result);
        setGeneratorDialogOpen(true);
        if (result.value) {
          //console.log("The result value is " + result.value);
          setHeadline(result.value);
          generateQr(result.value);
        } else {
          setDisplayCodeGenerator(true);
        }
      } else {
        setGeneratorDialogOpen(true);
        setDisplayCodeGenerator(true);
      }
    });
  };

  const verifyCode = async () => {
    try {
      setVerifyingCode(true);
      const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ signatureCode: verificationCode }),
      };

      const result = await fetch(
        "https://us-central1-reallyme-66efd.cloudfunctions.net/app/signature/verify",
        requestOptions
      );

      //console.log("RESULT");
      //console.log(result);

      let error = "";
      let userDetails;
      if (result.status === 403) {
        error = "Verification code expired";
      } else if (result.status === 404) {
        error = "Verification code not found";
      } else if (result.status === 200) {
        //console.log("Use 2000");
        error = "";
        userDetails = await result.json();
      }

      setVerifyingCode(false);
      setVerificationCodeResult(userDetails);
      setVerificationCodeError(error);
    } catch (error) {
      //console.log("Catch error");
      //console.log(error);
      setVerifyingCode(false);
      setVerificationCodeError("Failed to verify");
    }
  };

  const appendSignature = () => {
    const signature = generatedQrResponse;

    window.Office.context.mailbox.item.body.prependAsync(
      `<table style="width: 100%; background-color: #eee; padding: 10px; border-radius: 4px">
      <tr>
        <td>
        <img src="${signature.url}"/>
      </td>
        <td style="padding-left: 10px">
          <i>You can verify the authenticity of this message by using this biometric signature code: <b>#%RMC=${signature.signature}</b> on the reallyMe Add-on for GSuite and Outlook or on the reallyMe Mobile App or website.</i>
        </td>
      </tr>
    </table>`,
      { coercionType: window.Office.CoercionType.Html },
      (result) => {
        window.Office.context.mailbox.item.subject.setAsync(
          headline,
          (result) => {
            restoreDefaults();
          }
        );
      }
    );
  };

  const toggleDrawer = (open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    setDrawerOpen(open);
  };

  const handleGeneratorDialogClose = () => {
    setGeneratorDialogOpen(false);
  };
  const handleGeneratorInfoDialogClose = () => {
    setGeneratorInfoDialogOpen(false);
  };

  const handleGeneratorInfoDialogOpen = () => {
    setGeneratorInfoDialogOpen(true);
  };

  const handleVerificationDialogInfoClose = () => {
    setVerificationInfoDialogOpen(false);
  };

  const handleVerificationDialogInfoOpen = () => {
    setVerificationInfoDialogOpen(true);
  };

  const handleGenerateDialogOpen = () => {
    restoreDefaults();
    setGeneratorDialogOpen(true);
    handleGenerate();
  };
  const handleQrDialogClose = () => {
    setQrDialogOpen(false);
  };

  const restoreDefaults = () => {
    setVerifyingMobile(false);
    setDisplayVerificationSuccess(false);
    setDisplayCodeGenerator(false);
    setGeneratingQr(false);
    setGeneratedQrResponse(null);
    setHeadline("");
    setErrorMessage("");
    setGeneratorDialogOpen(false);
    setQrDialogOpen(false);
    setfaceVoiceDialogOpen(false);
    setShowVoiceEnrol(false);
    setShowFaceEnrol(false);
  };

  const anchor = "drawer";
  let codeGenerator = displayCodeGenerator ? (
    <Grid container direction="column" justify="center" alignItems="center">
      <input
        type="text"
        name="headline"
        placeholder="Headline"
        onChange={(e) => {
          setHeadline(e.target.value);
        }}
        value={headline}
      />
      <div
        className="btn"
        onClick={(e) => {
          generateQr();
        }}
      >
        Generate Signature
      </div>
    </Grid>
  ) : null;

  const verificationSuccessDisplay = displayVerificationSuccess ? (
    <ProcessAnimation status="success" />
  ) : null;
  const verificationFailureDisplay = displayVerificationFailure ? (
    <ProcessAnimation status="failure" />
  ) : null;

  const progressDisplay =
    creating || generatingQr || verifyingMobile ? (
      <Grid container direction="column" justify="center" alignItems="center">
        <CircularProgress style={{ color: "#9b27af" }} />
        <span style={{ textAlign: "center" }}>
          {creating ? "loading..." : ""}
          {generatingQr ? "Generating Qr Code..." : ""}
          {verifyingMobile
            ? "Please Verify your identity on your mobile device.."
            : ""}
        </span>
      </Grid>
    ) : null;
  const generatorDialog = (
    <Dialog
      onClose={handleGeneratorDialogClose}
      aria-labelledby="simple-dialog-title"
      open={generatorDialogOpen}
    >
      <DialogTitle id="simple-dialog-title">Enter Subject</DialogTitle>
      <div style={{ padding: "10px" }}>
        {progressDisplay}
        {verificationSuccessDisplay}
        {verificationFailureDisplay}
        {codeGenerator}
      </div>
    </Dialog>
  );

  const VerificationDialogContent = () => {
    if (verifyingCode) {
      return (
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          className="holder"
        >
          <CircularProgress style={{ color: "#9b27af" }} />
          Verifying code...
        </Grid>
      );
    } else if (verificationCodeError) {
      return (
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          className="holder"
        >
          <ProcessAnimation status="failure" />
          {verificationCodeError}
        </Grid>
      );
    } else if (verificationCodeResult) {
      return (
        <CodeVerificationCard result={verificationCodeResult} isDialog={true} />
      );
    } else {
      return (
        <div></div>
        //This option wasnt working - FOr reasons I cannot explain, the textfield has issues with setState on change
        //I had to create it in the dialog and comment this out to understand it later.
        // <Grid
        //   container
        //   direction="column"
        //   justify="center"
        //   alignItems="center"
        //   className="holder"
        // >
        //   <DialogTitle id="simple-dialog-title">
        //     Enter Code {verificationCode}
        //   </DialogTitle>
        //   <input
        //     type="text"
        //     name="verificationCode"
        //     placeholder="Verification Code"
        //     onChange={(e) => {
        //       //console.log(e);
        //       setVerificationCode(e.target.value);
        //     }}
        //     // onChange={(e) => {
        //     //   //console.log(e.target.value);
        //     //   setVerificationCode(e.target.value);
        //     // }}
        //     // value={verificationCode}
        //   />
        //   <div
        //     className="btn"
        //     onClick={(e) => {
        //       verifyCode();
        //     }}
        //   >
        //     Verify
        //   </div>
        // </Grid>
      );
    }
  };
  const verificationDialog = (
    <Dialog
      onClose={() => setVerificationDialogOpen(false)}
      aria-labelledby="simple-dialog-title"
      open={verificationDialogOpen}
      style={{ padding: "10px" }}
    >
      {!verifyingCode && !verificationCodeError && !verificationCodeResult ? (
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          className="holder"
        >
          <DialogTitle id="simple-dialog-title">Enter Code</DialogTitle>
          <input
            type="text"
            name="verificationCode"
            placeholder="Verification Code"
            onChange={(e) => {
              setVerificationCode(e.target.value);
            }}
            value={verificationCode}
          />
          <div
            className="btn"
            onClick={(e) => {
              verifyCode();
            }}
          >
            Verify
          </div>
        </Grid>
      ) : null}
      <VerificationDialogContent />
    </Dialog>
  );
  const generatorInfoDialog = (
    <Dialog
      onClose={handleGeneratorInfoDialogClose}
      aria-labelledby="simple-dialog"
      open={generatorInfoDialogOpen}
    >
      <InfoDisplay
        title="What is a Biometric Signature"
        description="A Biometric Signature is a code that is uniquely generated from your biometrics on reallyMe.You can utilize this signature to protect your emails and other communications, and the recipients can verify their authenticity as coming directly from you."
      />
    </Dialog>
  );
  const verificationInfoDialog = (
    <Dialog
      onClose={handleVerificationDialogInfoClose}
      aria-labelledby="simple-dialog"
      open={verificationInfoDialogOpen}
    >
      <InfoDisplay
        title="Verifying a Biometric Signature"
        description="You can verify a reallyMe Biometric Signature by just opening the message that contains this signature and launching reallyMe Addin on Outlook or Gmail. This shows you details about the authentic sender and the time it was generated."
      />
    </Dialog>
  );
  const qrDisplayDialog = generatedQrResponse ? (
    <Dialog
      onClose={handleQrDialogClose}
      aria-labelledby="simple-dialog-title"
      open={qrDialogOpen}
    >
      <Grid direction="column" container alignItems="center" justify="center">
        <div className="verification-icon-holder">
          <VerifiedUserIcon style={{ color: "#a94dbd" }} />
        </div>
        <span className="qr-date">
          Your Biometric Signature Code Expires On{" "}
          {moment(generatedQrResponse.expiry_date).format(
            "MMMM Do YYYY, h:mm a"
          )}
        </span>
        <img
          src={generatedQrResponse.url}
          width="150px"
          height="150px"
          alt="qrImage"
        />
        <span className="qr-code">{generatedQrResponse.signature}</span>
        <span className="qr-notice">
          Click on the button below to append this signature to your message.
        </span>
        <span className="btn" onClick={handleAppendSignature}>
          Append
        </span>
      </Grid>
    </Dialog>
  ) : null;

  const DialogContent = () => {
    if (showFaceEnrol) {
      //console.log("showFace");
      return (
        <FaceEnrollmentDialog
          action="verify"
          onSuccess={handleFaceEnrollmentSuccess}
        />
      );
    } else if (showVoiceEnrol) {
      //console.log("showVoice");
      return (
        <VoiceEnrollmentDialog
          action="verify"
          onSuccess={handleVoiceEnrollmentSuccess}
        />
      );
    }
    return <div>.</div>;
  };

  return (
    <React.Fragment key={anchor}>
      <div>
        {generatorDialog}
        {generatorInfoDialog}
        {verificationInfoDialog}
        {qrDisplayDialog}
        {verificationDialog}

        <Dialog
          onClose={() => setfaceVoiceDialogOpen(false)}
          aria-labelledby="simple-dialog"
          open={faceVoiceDialogOpen}
        >
          <DialogContent />
        </Dialog>
        <IconButton
          onClick={toggleDrawer(true)}
          aria-label="menu"
          style={{
            color: "white",
            position: "absolute",
            top: "10px",
            left: "10px",
            zIndex: "5",
          }}
        >
          <Menu />
        </IconButton>
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          className="header"
        >
          <Avatar
            alt="avatar"
            src={user.image_url}
            style={{ width: "60px", height: "60px", marginBottom: "10px" }}
          />

          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing="3"
            className="name"
          >
            <span style={{ fontSize: "1.2rem", marginRight: "5px" }}>
              {user.username || "no username"}
            </span>
            <img src={BadgeIcon} width="20px" height="20px" alt="badge" />
          </Grid>
          <span className="email">{user.primary_email}</span>
        </Grid>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          className="top-info"
          spacing="5"
        >
          <Grid item>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <span> Membership Status</span>
              <span> {user.trial_user ? "Basic" : "Pro"}</span>
            </Grid>
          </Grid>
          <Grid item>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <span> Available Authentications</span>
              <span>{user.subscribed ? "unlimited" : user.mail_sends}</span>
            </Grid>
          </Grid>
        </Grid>

        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
          style={{ marginTop: "25px" }}
        >
          <Grid item xs={4}>
            <CountDetail
              icon={SignatureIcon}
              title="Signatures"
              count={user.subscribed ? "unlimited" : user.mail_sends}
              description="Available Biometric Codes"
            />
          </Grid>
          <Grid item xs={4}>
            <CountDetail
              icon={VerificationIcon}
              title="Verifications"
              count={verificationCount === null ? "..." : verificationCount}
              description="Liveness Verification"
            />
          </Grid>
          <Grid item xs={4}>
            <CountDetail
              icon={OrganizationIcon}
              title="Organization"
              count={organizationCount === null ? "..." : organizationCount}
              description={
                organizationCount !== null &&
                userOrganization &&
                organizationCount > 0
                  ? userOrganization.organization
                  : "Your Organization"
              }
            />
          </Grid>
        </Grid>
        <div style={{ padding: "10px" }}>
          <div class="title">Quick Action</div>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <div class="btn perform-btn" onClick={handleGenerateDialogOpen}>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing="2"
              >
                <Grid item>
                  <span>Generate Biometric Signature</span>
                </Grid>
                <Grid item>
                  <img
                    src={SignatureWhiteIcon}
                    alt="sig"
                    width="20px"
                    height="20px"
                  />
                </Grid>
              </Grid>
            </div>
            <div
              class="btn perform-btn verify"
              onClick={() => {
                setVerifyingCode(false);
                setVerificationCode("");
                setVerificationCodeResult(null);
                setVerificationCodeError("");
                setVerificationDialogOpen(true);
              }}
            >
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
                spacing="3"
              >
                <Grid item>
                  <span>Verify Biometric Signature</span>
                </Grid>
                <Grid item>
                  <img
                    src={VerificationWhiteIcon}
                    alt="sig"
                    width="20px"
                    height="20px"
                  />
                </Grid>
              </Grid>
            </div>
          </Grid>
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing="1"
          >
            <Grid item xs={6}>
              <QuickAction
                icon={SignatureWhiteIcon}
                onActionClick={handleGeneratorInfoDialogOpen}
                title="Biometric Signatures"
                description="Learn about reallyMe Biometric Signatures"
                buttonText="Learn more"
                color="#4B8F8C"
              />
            </Grid>
            <Grid item xs={6}>
              <QuickAction
                icon={VerificationWhiteIcon}
                onActionClick={handleVerificationDialogInfoOpen}
                title="Verify a Signatures"
                description="Click on the Verify Button to get started"
                buttonText="Learn more"
                color="#FF8811"
              />
            </Grid>
          </Grid>
        </div>
      </div>

      <SwipeableDrawer
        anchor="left"
        open={drawerOpen}
        onClose={toggleDrawer(false)}
        onOpen={toggleDrawer(true)}
      >
        {/* <Link to="/signature-list" style={{ color: "black", fontSize: "20px" }}>
          Biometric Signatures
        </Link> */}

        <List>
          <ListItem
            button
            key="logout"
            onClick={() => {
              //console.log("logout user");
              fire.auth().signOut();
              window.location.href = "/";
            }}
          >
            <ListItemIcon>
              <VerifiedUserIcon />
            </ListItemIcon>
            <ListItemText primary="Logout" />
          </ListItem>
        </List>
      </SwipeableDrawer>
    </React.Fragment>
  );
};

export default Home;
