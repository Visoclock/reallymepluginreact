import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Grid, Card, CardContent, Dialog } from "@material-ui/core";
import VoiceEnrollmentDialog from "./sub/VoiceEnrollmentDialog";
import FaceEnrollmentDialog from "./sub/FaceEnrollmentDialog";
import CircularProgress from "@material-ui/core/CircularProgress";
// import fire from "../../../config/fire";
import fire from "./../config/fire";

const Enrol = (props) => {
  //console.log("enroll log..");
  //console.log(props.location.state);

  const history = useHistory();

  const gottenUser = props.location.state.user;
  //console.log("ENROL USERRRR");
  //console.log(gottenUser);
  const [user, setUser] = useState(gottenUser);
  // const [enrolled, setEnrolled] = useState(false);
  const [enrollmentDialogOpen, setEnrollmentDialogOpen] = useState(false);
  const [showFaceEnrol, setShowFaceEnrol] = useState(false);
  const [showVoiceEnrol, setShowVoiceEnrol] = useState(false);
  const [loading, setLoading] = useState(false);
  const [profile, setProfile] = useState(null);
  const [errorMessage, setErrorMessage] = useState("");

  const createProfile = async () => {
    try {
      setLoading(true);
      setEnrollmentDialogOpen(false);
      //console.log("Creating profile....");
      const userToken = await fire.auth().currentUser.getIdToken();
      const profileRequestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${userToken}`,
        },
        body: JSON.stringify({ locale: "en-US" }),
      };

      //console.log("CREATING PROFILE .......................");
      const profileRequestResult = await fetch(
        `https://us-central1-reallyme-66efd.cloudfunctions.net/app/speaker/dependent/create-profile`,
        profileRequestOptions
      );

      const profile = await profileRequestResult.json();
      //console.log("CREATED PROFILE .......................");
      //console.log(profile);
      setProfile(profile);

      setLoading(false);
      setEnrollmentDialogOpen(true);
      setShowVoiceEnrol(true);
      return true;
    } catch (error) {
      //console.log(error);
      setLoading(false);
      setErrorMessage("Failed to create Verification profile");
      return false;
    }
  };

  const handleUserEnrollmentNew = async (e) => {
    setEnrollmentDialogOpen(true);
    if (!user.faceEnrolled) {
      setShowFaceEnrol(true);
    } else if (!user.enrolled) {
      //console.log("allow voice...........");
      await createProfile();
    }
  };
  const handleFaceEnrollmentSuccess = async () => {
    setUser({ ...user, face_enrolled: true });
    if (!user.enrolled) {
      //console.log("Aloow voice...........");
      setShowFaceEnrol(false);
      await createProfile();
    } else {
      history.push("/home", { user: user });
    }
  };
  const handleVoiceEnrollmentSuccess = () => {
    setUser({ ...user, voice_enrolled: true });
    history.push("/home", { user: user });
  };

  const DialogContent = () => {
    if (showFaceEnrol) {
      //console.log("FACE STUFF MOUNTING.....");
      return (
        <FaceEnrollmentDialog
          action="enrol"
          onSuccess={handleFaceEnrollmentSuccess}
        />
      );
    } else if (showVoiceEnrol) {
      //console.log("VOICE STUFF MOUNTING.......");
      return (
        <VoiceEnrollmentDialog
          action="enrol"
          profileId={profile && profile.profileId}
          onSuccess={handleVoiceEnrollmentSuccess}
        />
      );
    }
    return <div>.</div>;
  };

  const loadingDisplay = loading ? (
    <CircularProgress style={{ color: "#9b27af" }} />
  ) : null;
  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      style={{ minHeight: "90vh" }}
    >
      <Dialog
        onClose={() => setEnrollmentDialogOpen(false)}
        aria-labelledby="simple-dialog"
        open={enrollmentDialogOpen}
      >
        <DialogContent />
      </Dialog>
      <Card style={{ margin: "20px", borderRadius: "20px" }}>
        <CardContent>
          <Grid
            direction="column"
            container
            alignItems="center"
            justify="center"
          >
            <div className="name-holder">User Enrollment</div>
            {errorMessage}
            <div class="welcome-info">
              <span>
                Your voice and your face will need to be enrolled for
                recognition. Click on the <b>Enrol</b> button to enrol
              </span>
            </div>
            {loadingDisplay}
            <div class="btn" onClick={handleUserEnrollmentNew}>
              Enrol
            </div>
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default Enrol;
