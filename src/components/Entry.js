import React, { useState, useEffect } from "react";
import Home from "./Home";
import Login from "./Login";
import fire from "../config/fire";
import Splash from "./Splash";
import { Route, Switch, Redirect } from "react-router-dom";
import { useHistory } from "react-router-dom";

const Entry = (props) => {
  //console.log("--props in entry--");
  //console.log(props.location.state);
  const [user, setUser] = useState(null);
  const [checkingAuthentication, setCheckingAuthentication] = useState(true);
  const [fetchingAuthenticatedUser, setFetchingAuthenticatedUser] = useState(
    false
  );
  const [redirectLogin, setRedirectLogin] = useState(false);
  const [redirectEnrol, setRedirectEnrol] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    const location = props.location;
    const user = location.state && location.state.user;
    //console.log("entry user --------");
    //console.log(user);
    setErrorMessage("");
    if (user) {
      setCheckingAuthentication(false);
      setUser(user);
      if (!user.enrolled) {
        setRedirectEnrol(true);
      }
    } else {
      setCheckingAuthentication(true);

      fire.auth().onAuthStateChanged(async (user) => {
        if (user) {
          //console.log("user-----");
          //console.log(user);
          setFetchingAuthenticatedUser(true);
          try {
            const userId = fire.auth().currentUser.uid;

            const userDoc = await fire
              .firestore()
              .collection("users")
              .doc(userId)
              .get();

            const gottenUser = userDoc.data();
            setFetchingAuthenticatedUser(false);
            //console.log("fetched user....");
            //console.log(gottenUser);
            setUser(gottenUser);
            if (!gottenUser.enrolled || !gottenUser.face_enrolled) {
              setRedirectEnrol(true);
            }
          } catch (error) {
            //console.log("error happened");
            //console.log(error);
            setErrorMessage("Failed to fetch User details check your network");
            setFetchingAuthenticatedUser(false);
          }
        } else {
          setRedirectLogin(true);
        }
        setCheckingAuthentication(false);
      });
    }
  }, [props.location]);

  //TODO: uncomment
  if (checkingAuthentication || fetchingAuthenticatedUser) {
    return <Splash title="loading..." />;
  } else if (redirectLogin) {
    return <Redirect to={{ pathname: "/login" }} />;
  } else if (redirectEnrol) {
    return <Redirect to={{ pathname: "/enrol-profile", state: { user } }} />;
  } else if (errorMessage) {
    return <Splash title={errorMessage} />;
  } else {
    return <Redirect to={{ pathname: "/home", state: { user } }} />;
  }

  //TODO: remove
  // if (checkingAuthentication || fetchingAuthenticatedUser) {
  //   return <Splash title="loading..." />;
  // } else {
  //   return <Redirect to={{ pathname: "/enrol", state: { user } }} />;
  // }
};

export default Entry;
