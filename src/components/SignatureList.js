import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import fire from "../config/fire";
import Grid from "@material-ui/core/Grid";
import "./SignatureList.css";
import { IconButton, Typography, Toolbar, AppBar } from "@material-ui/core";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import SignatureItem from "./sub/SignatureItem";

const SignatureList = (props) => {
  const [loading, setLoading] = useState(true);
  const [errorMessage, setErrorMessage] = useState("");
  const [signatures, setSignatures] = useState([]);

  const history = useHistory();

  useEffect(() => {
          
    const fetchSignatures = async () => {
        try {
          setLoading(true);
    
          const signatureDoc = await fire
            .firestore()
            .collection("signature")
            .where("user_id", "==", props.user.uuid)
            .orderBy("creation_date_time", "desc")
            .get();
          //console.log(signatureDoc);
          if (signatureDoc.empty) {
            setSignatures([]);
          } else {
            let gottenSignatures = [];
            signatureDoc.forEach((doc) => {
              gottenSignatures.push({
                ...doc.data(),
                creation_date_time: doc.data().creation_date_time.toDate(),
                ...(doc.data().expiry_date_time && {
                  expiry_date_time: doc.data().expiry_date_time.toDate(),
                }),
              });
            });
            setSignatures(gottenSignatures);
          }
          setLoading(false);
        } catch (error) {
          //console.log(error);
          setLoading(false);
          setErrorMessage("error getting signatures");
        }
      };
      fetchSignatures();

  }, [props.user.uuid]);

  const progress = loading ? <div>loading item..</div> : null;
  const SignatureList = () => {
    if (!loading && !errorMessage) {
      if (Array.isArray(signatures) && signatures.length !== 0) {
        const signatureItems = signatures.map((signature) => (
          <SignatureItem signature={signature} key={signature.code}/>
        ));
        return <div>{signatureItems}</div>;
      } else {
        return <div>Empty Item list</div>;
      }
    }
    return null;
  };

  return (
    <div>
      <AppBar position="relative" style={{ backgroundColor: "#9b27af" }}>
        <Toolbar variant="dense">
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={() => {
              history.goBack();
            }}
          >
            <ArrowBackIcon />
          </IconButton>
          <Typography variant="h6">Signature List</Typography>
        </Toolbar>
      </AppBar>
      <div>{progress}</div>
      <SignatureList />
    </div>
  );
};

export default SignatureList;
