import React, { useState, useEffect } from "react";
import Home from "./components/Home";
import Login from "./components/Login";
import CodeVerification from "./components/CodeVerification";
import SignatureList from "./components/SignatureList";
import SignatureUserList from "./components/SignatureUserList";
import Error from "./components/Error";
import Entry from "./components/Entry";
import Enrol from "./components/Enrol";
import EnrolProfile from "./components/EnrolProfile";
import { Route, Switch } from "react-router-dom";

const App = (props) => {
  return (
    <main>
      <Switch>
        <Route path="/home" component={Home} />
        <Route path="/verify" component={CodeVerification} />
        <Route path="/login" component={Login} />
        <Route path="/enrol-profile" component={EnrolProfile} />
        <Route path="/enrol" component={Enrol} />
        <Route path="/signature-list" render={(props) => <SignatureList />} />
        <Route
          path="/signature-users/:signatureCode"
          render={(props) => <SignatureUserList />}
        />
        {/* <Route path="/" component={EnrolProfile} /> */}
        <Route path="/" component={Entry} />
        {/* <Route
          path="/"
          render={(props) =>
            user !== null ? (
              <Home user={user} {...props} />
            ) : (
              <Redirect
                to={{ pathname: "/login", state: { from: props.location } }}
              />
            )
          }
        /> */}

        <Route component={Error} />
      </Switch>
    </main>
  );
};

export default App;
