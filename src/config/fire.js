import firebase from "firebase";
// import "firebase/firestore";
// import "firebase/auth";

import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyANOP0TXFDo16boZkiqtiGI6v2NAidsc18",
  authDomain: "reallyme-66efd.firebaseapp.com",
  databaseURL: "https://reallyme-66efd.firebaseio.com",
  projectId: "reallyme-66efd",
  storageBucket: "reallyme-66efd.appspot.com",
  messagingSenderId: "506824883755",
  appId: "1:506824883755:web:fc18583ad419550c0f346e",
  measurementId: "G-28PQX2VQK1",
};

const fire = firebase.initializeApp(firebaseConfig);
export default fire;
