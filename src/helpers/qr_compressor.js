import fire from "../config/fire";
import Compressor from "compressorjs";

const dataURLtoFile = (dataurl, filename) => {
  var arr = dataurl.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
};

const compressQrResponse = (qrResponse, urlCallback) => {
  const result = dataURLtoFile(qrResponse.QRCode, "qr_image");
  new Compressor(result, {
    quality: 0.9,
    width: 20,
    height: 20,
    async success(result) {
      console.log("new file");
      console.log(result);
      try {
        const fileName =
          Math.random().toString().slice(2, 15) +
          Math.random().toString().slice(2, 15);
        const imageRef = await fire
          .storage()
          .ref(`_mini_qr/${fileName}.png`)
          .put(result);
        const imageUrl = await imageRef.ref.getDownloadURL();
        console.log(imageUrl);
        urlCallback(imageUrl);
      } catch (error) {
        //failed
        console.log(error);
        console.log(error);
        urlCallback(false);
      }
    },
    error(err) {
      throw new Error("Could not compress Image");
    },
  });
};

export default compressQrResponse;
