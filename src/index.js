import "react-app-polyfill/ie11";
import "react-app-polyfill/stable";
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter} from "react-router-dom";
import "./index.css";
import App from "./App";

let isOfficeInitialized = true;

const render = (Component) => {
  ReactDOM.render(
    <BrowserRouter>
      <Component isOfficeInitialized={isOfficeInitialized} />
    </BrowserRouter>,
    document.getElementById("container")
  );
};

window.Office.onReady(function (info) {
  isOfficeInitialized = true;
  render(App);
});

window.Office.initialize = function () {};
